import { Component, OnInit } from '@angular/core';
import {HomeService}  from '../home.service';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
@Component({
  selector: 'app-add-ride',
  templateUrl: './add-ride.component.html',
  styleUrls: ['./add-ride.component.css'],

})
export class AddRideComponent implements OnInit {

  ridedetails={
    id:null,
    empid:null,
    vehicletype:{id:null,text:null},
    vehiclenum:null,
    vacantseats:null,
    time:{hour:null, minute:null,second:null},
    pickup:null,
    drop:null
  };
  meridian = true;
  public items:Array<any> = [{'id':1,text:'Bike'},{'id':2,text:'Car'}];
  public countlist:Array<any> = [{'id':1,text:'1'},{'id':2,text:'2'},{'id':3,text:'3'},{'id':4,text:'4'},{'id':5,text:'5'},{'id':6,text:'6'}];
  constructor(private _homeService:HomeService, private router:Router,private notService:NotificationsService) { }

  ngOnInit() {
  }

  OnSubmit(form:any){
    console.log(this.ridedetails);
    this._homeService.addRideDetails(this.ridedetails).subscribe((data)=>{
      console.log(data);
      this.notService.create('Success', 'Ride Added Successfully');
      this.router.navigateByUrl('/');
    });


  }

  Reset(){
    this.ridedetails={
      id:null,
      empid:null,
      vehicletype:{id:null,text:null},
      vehiclenum:null,
      vacantseats:null,
      time:{hour:null, minute:null,second:null},
      pickup:null,
      drop:null
    };

  }

}
