import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AddRideComponent } from './add-ride/add-ride.component';
import { FindRideComponent } from './find-ride/find-ride.component';
import { HomeComponent } from './home/home.component';
import {NgSelectModule}  from '@ng-select/ng-select';
import {NgbModule}  from '@ng-bootstrap/ng-bootstrap';
import {FormsModule}  from '@angular/forms';
import {HttpClientModule}  from '@angular/common/http';
import { SimpleNotificationsModule } from 'angular2-notifications';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent},
  { path: 'find-ride', component: FindRideComponent},
  { path: 'add-ride', component: AddRideComponent},
  
];
@NgModule({
  declarations: [
    AppComponent,
    AddRideComponent,
    FindRideComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,RouterModule.forRoot(appRoutes),NgSelectModule,FormsModule,NgbModule,HttpClientModule,
    SimpleNotificationsModule.forRoot(),BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
