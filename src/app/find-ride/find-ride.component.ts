import { Component, OnInit } from '@angular/core';
import {HomeService}  from '../home.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-find-ride',
  templateUrl: './find-ride.component.html',
  styleUrls: ['./find-ride.component.css']
})
export class FindRideComponent implements OnInit {
  vehicletype:'';
  constructor(private _homeservice:HomeService,private _notService:NotificationsService) { }
  ridelist:any;
  originalridelist:any;
  currenthour:any;
  currentminute:any;
  ngOnInit() {
    this.currenthour=new Date().getHours();
    this.currentminute=new Date().getMinutes();
    this._homeservice.getAllRides().subscribe((response)=>{
      this.originalridelist=response;
     //this.filterByTime();
      
      this.ChangeVehicleType('Any');
    });

  }

  filterByTime(){
    if(this.originalridelist.length>0){

    

    this.ridelist=this.originalridelist.filter(a=>(a.time.hour-this.currenthour<=1 && a.time.hour-this.currenthour>=-1) && 
      (a.time.minute-this.currentminute<=59 && a.time.minute-this.currentminute>=-59) && a.vacantseats>0);
    }else{
      this.ridelist=[];
    }
  }

  empid:any=null;
  myride:any;
  SelectedRide(ride:any){

  this.myride=ride;

  }
  BookCab(){
    this.myride.vacantseats-=1;
    this._homeservice.bookcab({id:null,rideid:this.myride.id,empid:this.empid}).subscribe((response)=>
  {
    this._notService.create('Success', 'Ride Booked Successfully');
  });
    this._homeservice.updateseat(this.myride).subscribe(
      (response)=>
  {
    this._notService.create('Update', 'Vacant Seats Updated');
  }
    );
    this.ChangeVehicleType(this.vehicletype);
  }

  ChangeVehicleType(type:any){
    this.vehicletype=type;
    if(type=='Any'){
      this.filterByTime();

    }else{
      this.filterByTime();
      this.ridelist=this.ridelist.filter(x=>x.vehicletype.text==type);
    }

  }

}
