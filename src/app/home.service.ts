import { Injectable } from '@angular/core';
import {catchError} from 'rxjs/operators';
import {HttpClient,HttpHeaders} from '@angular/common/http';
const httpOptions={
  headers:new HttpHeaders({
    'Content-Type':'application/json'
  })
}
@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(private http:HttpClient) { }

  getAllRides(){
    return this.http.get("http://localhost:3000/rides");
  }
  updateseat(ride:any){
    return this.http.put("http://localhost:3000/rides/"+ride.id,ride,httpOptions);
  }
  bookcab(obj:any){
    return this.http.post("http://localhost:3000/bookings",obj,httpOptions);
  }

  addRideDetails(obj:any){

    return this.http.post("http://localhost:3000/rides",obj,httpOptions);
   }
}
